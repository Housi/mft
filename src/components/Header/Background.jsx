import styled from "styled-components";
import { isObject } from "utils/object";

const LinearGradient = ({ from, to, direction = 90 }) => {
  return `linear-gradient(${direction}deg, ${from} 0%, ${to} 100%)`;
};

const Background = styled.div`
  position: absolute;
  left: 0px;
  top: 0px;
  bottom: 0;
  right: 0;
  z-index: -1;
  ${({ color }) =>
    isObject(color)
      ? `
    background: ${LinearGradient(color)};
  `
      : `
    background-color: ${color};
`}
`;

export { LinearGradient, Background };
