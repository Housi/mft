import React from "react";
import styled from "styled-components";
import articles from "mocks/articles.js";

const Article = styled.article`
  display: flex;
  padding: 0.5em;
  &:not(:last-child) {
    border-bottom: 1px solid lightgray;
  }
`;

const Image = styled.img`
  width: 40vw;
  height: 30vw;
  object-fit: cover;
`;

const ArticleBody = styled.div`
  flex: 1 100%;
  padding: 0 0.5em;
`;

const ArticleListItem = ({ title, description, image }) => (
  <Article>
    {image && <Image src={image} alt={title} />}
    <ArticleBody>
      <h2>{title}</h2>
      <p>{description}</p>
    </ArticleBody>
  </Article>
);

const ArticleList = ({ section }) => {
  const sectionArticles = articles.filter(({ sections }) =>
    sections.includes(section)
  );

  return (
    <div>
      {sectionArticles.map(({ title, description, image }, key) => (
        <ArticleListItem
          title={title}
          description={description}
          image={image}
          key={key}
        />
      ))}
    </div>
  );
};

export default ArticleList;
