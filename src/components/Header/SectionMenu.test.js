import React from "react";
import { cleanup, render } from "@testing-library/react";
import "jest-styled-components";

import SectionMenu from "components/Header/SectionMenu";

const location = window.location;

afterEach(() => {
  global.window.location = location;
  cleanup();
});

test("It renders element with specified styles and text", () => {
  const text = ["section1", "section2", "section3", "section4", "section5"];

  const { container } = render(
    <SectionMenu
      text={text}
      color="pink"
      textSize="12px"
      activeColor="yellow"
    />
  );
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild).toHaveStyleRule("color", "pink");
  expect(container.firstChild).toHaveStyleRule("font-size", "12px");
  expect(container.firstChild.textContent).toBe(text.join(""));
});

test("It changes active (text match url) element border to given color", () => {
  const text = ["section1", "section2", "section3", "section4", "section5"];

  delete global.window.location;
  global.window.location = {
    pathname: "/section5",
    href: "http://my.test",
  };

  const { container } = render(
    <SectionMenu
      text={text}
      color="pink"
      textSize="12px"
      activeColor="yellow"
    />
  );
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild.lastChild).toHaveStyleRule(
    "border-bottom",
    "2px solid yellow"
  );
  expect(container.firstChild.firstChild).toHaveStyleRule(
    "border-bottom",
    "2px solid transparent"
  );
});
