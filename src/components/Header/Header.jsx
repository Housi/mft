import React, { useState } from "react";
import styled from "styled-components";
import defaultConfig from "./defaultConfig.js";
import { deepMerge } from "utils/object";

import MenuIcon from "assets/MenuIcon.jsx";
import { Background } from "components/Header/Background";
import SectionMenu from "components/Header/SectionMenu";
import { useScrollPosition } from "hooks/useScrollPosition";

const BurgerMenu = styled(MenuIcon)`
  height: 100%;
`;

const Wrapper = styled.header`
  position: -webkit-sticky; /* Safari */
  position: sticky;
  top: 0;
  width: 100%;
`;

const Section = styled.section`
  display: flex;
`;

const TopSection = styled(Section)`
  padding: 0.5em;
  height: ${({ size }) => size}px;
`;

const Logo = styled.img`
  height: ${({ size }) => size}px;
  flex: 1;
  object-fit: contain;
`;

const Header = ({ config }) => {
  const [showLinks, setShowLinks] = useState(true);
  const [showTopBar, setShowTopBar] = useState(true);

  const { logo, background, burgerMenu, sectionMenu } = deepMerge(
    defaultConfig,
    config
  );

  useScrollPosition(
    ({ nextPos }) => {
      const tresh1 = nextPos.y <= 400;
      const tresh2 = nextPos.y <= 800;
      if (tresh1 !== showLinks) setShowLinks(tresh1);
      if (tresh2 !== showTopBar) setShowTopBar(tresh2);
    },
    [showLinks, showTopBar],
    100
  );

  return (
    <Wrapper>
      {showTopBar && (
        <Background color={background.color} gradient={background.gradient} />
      )}
      <TopSection size={logo.size}>
        <BurgerMenu color={burgerMenu.color}></BurgerMenu>
        {showTopBar && <Logo src={logo.image} size={logo.size} alt="logo" />}
      </TopSection>
      {showLinks && (
        <SectionMenu
          text={sectionMenu.text}
          color={sectionMenu.color}
          textSize={sectionMenu.textSize}
          activeColor={sectionMenu.activeColor}
        />
      )}
    </Wrapper>
  );
};

export default Header;
