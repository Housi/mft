import { useRef, useLayoutEffect } from "react";

const isBrowser = typeof window !== `undefined`;

function getScrollPosition() {
  if (!isBrowser) return { x: 0, y: 0 };
  return { x: window.scrollX, y: window.scrollY };
}

export function useScrollPosition(fn, dependancies, delay) {
  const position = useRef(getScrollPosition());
  // eslint-disable-next-line
  let throttle = null;

  const cb = () => {
    const nextPos = getScrollPosition();
    fn({ prevPos: position.current, nextPos });
    position.current = nextPos;
    throttle = null;
  };

  useLayoutEffect(() => {
    const handleScroll = () => {
      // eslint-disable-next-line
      throttle = setTimeout(cb, delay);
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, dependancies);
}
