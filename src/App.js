import React from "react";
import Header from "components/Header/Header";
import config from "mocks/config.js";
import { useRoutes } from "hookrouter";
import ArticleList from "components/ArticleList";

function App() {
  const routes = {
    "/:section": ({ section }) => <ArticleList section={section} />,
  };
  const routeResult = useRoutes(routes);

  return (
    <div className="App">
      <Header config={config}></Header>
      {routeResult}
    </div>
  );
}

export default App;
