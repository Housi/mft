import React from "react";
import { cleanup, render } from "@testing-library/react";
import "jest-styled-components";

import Header from "components/Header/Header";

afterEach(cleanup);

test("It renders header with default config, and all elements visible", () => {
  const { container } = render(<Header />);
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild.childNodes.length).toBe(3);
});
