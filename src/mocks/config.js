export default {
  background: {
    color: { from: "LIGHTPINK", to: "SILVER" },
  },
  burgerMenu: {
    color: "RED",
  },
  logo: {
    size: 32,
  },
  sectionMenu: {
    activeColor: "RED",
  },
};
