export default {
  background: {
    color: "WHITE",
  },
  burgerMenu: {
    color: "CRIMSON",
  },
  logo: {
    size: 36,
    image: "/logo-marfeel.svg",
  },
  sectionMenu: {
    textSize: 14,
    color: "BLACK",
    text: [
      "home",
      "politics",
      "opinions",
      "culture",
      "business",
      "technology",
      "people",
    ],
  },
};
