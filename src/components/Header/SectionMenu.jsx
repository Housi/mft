import React, { useEffect } from "react";
import styled from "styled-components";
import { A, navigate } from "hookrouter";

const MenuWrapper = styled.section`
  display: flex;
  color: ${({ color }) => color};
  font-size: ${({ textsize }) => textsize};
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const Navlink = styled(A)`
  padding: 0 1em;
  color: inherit;
  text-decoration: none;
  text-transform: uppercase;
  border-bottom: 2px solid ${({ border }) => border};
`;

const SectionMenu = ({ text, color, activeColor, textSize }) => {
  const path = window.location.pathname.split("/")[1];
  const isActive = (section) => path === section;
  useEffect(() => {
    if (!text.includes(path)) navigate(text[0]);
  }, [text, path]);
  return (
    <MenuWrapper color={color} textsize={textSize}>
      {text.map((link, key) => (
        <Navlink
          border={isActive(link) ? activeColor : "transparent"}
          href={`/${link}`}
          key={key}
        >
          {link}
        </Navlink>
      ))}
    </MenuWrapper>
  );
};

export default SectionMenu;
