export default [
  {
    sections: ["home", "sports"],
    title: "Finally, A VR Headset That Gets Nausea Right",
    description:
      "Virtual reality companies are in a race to see how fast they can make users vomit. Finally a leader emerges.",
    image: "https://live.staticflickr.com/969/42247657411_3e02e3506e_z.jpg",
  },
  {
    sections: ["home", "opinions", "politics"],
    title: "Laurie Metcalf Was Hiding in Plain Sight",
    description:
      "“A unicorn, a monster, a phoenix, a machine, a heavyweight fighter, an astronaut, a superhero, a thoroughbred, a home-run hitter, a waitress juggling ‘16 entrees, 42 starters, 16 desserts,’ a jazz virtuoso, LeBron James, Magellan, Snuffleupagus. The actress Laurie Metcalf has been compared to all of these things.”",
  },
  {
    sections: ["home", "politics", "culture"],
    image: "https://live.staticflickr.com/3593/3778730672_0991c46cf3_z.jpg",
    title:
      "Flint Residents Learn To Harness Superpowers, But Trump Gets Away Again",
    description:
      "FLINT—They developed superpowers after years of drinking from a lead-poisoned water supply. But just having incredible abilities doesn't make them superheroes. Not yet.",
  },
  {
    sections: ["home", "opinions", "business"],
    title:
      "Once Thriving Cheeto Driven To Extinction By Unregulated Snack Food Industry",
    description:
      "CHESTER, IA—Where once the cheeto could be found in great numbers, filling the crisp autumn air with a dangerously cheesy aroma, today there isn’t a trace of fluorescent orange to be seen, as overharvesting has all but eliminated the",
    image: "https://live.staticflickr.com/6007/5906449345_ff59237385_z.jpg",
  },
  {
    sections: ["home", "sports", "politics"],
    title: "The Same River Twice",
    description: "The story of rivers and relationships.",
  },
  {
    sections: ["home", "opinions", "politics"],
    image: "https://live.staticflickr.com/596/21445252278_44eff3e912_z.jpg",
    title:
      "Eldest Grandson Reigns Supreme With 3 School Pictures On Nana’s Fridge",
    description:
      "ROCKFORD, MI—After months of intense combat and speculation, James once again emerged as the favored grandson, with a record-setting four pictures on Nana's fridge.",
  },
  {
    sections: ["home", "politics", "culture"],
    title: "The Same River Twice",
    description: "The story of rivers and relationships.",
  },
  {
    sections: ["home", "opinions", "business", "technology"],
    title: "Vodka Nation",
    description:
      "How the spirit became a billion-dollar business. Michael Roper, owner of Chicago’s Hopleaf bar and restaurant, recalls what bartending was like in the early seventies.While Smirnoff was considered top shelf, he remembers lesser varieties such as Nikolai, Arrow, Wolfschmidt, and another brand that was then ubiquitous called Mohawk.“Mohawk was cheap, cheap, cheap, ” Roper remembers.“Mohawk had a factory just outside Detroit along the expressway and .  .  .all their products were made there.It’s almost like they turned a switch—whiskey, vodka, gin.And it was all junk.” Still, by 1976, vodka had surpassed bourbon and whiskey as the most popular spirit in America.Roper attributes vodka’s rise partially to women, who started drinking more spirits and ordering them on their own: “Women were not going to like Scotch—that was for cigar - smoking burly men, ” he speculates.“And .  .  .it was unladylike to drink Kentucky whiskey.But it was considered somewhat ladylike to have a fancy cocktail with an olive in it.” He also remembers when a salesman first brought Miller Lite into his bar, explaining “it’s for women.” In a similar vein, Roper considers vodka a low- calorie option with “a less challenging flavor.”",
  },
  {
    sections: ["home", "sports", "politics"],
    title: "Finally, A VR Headset That Gets Nausea Right",
    description:
      "Virtual reality companies are in a race to see how fast they can make users vomit. Finally a leader emerges.",
    image: "https://live.staticflickr.com/969/42247657411_3e02e3506e_z.jpg",
  },
  {
    sections: ["opinions", "sports"],
    title: "Laurie Metcalf Was Hiding in Plain Sight",
    description:
      "“A unicorn, a monster, a phoenix, a machine, a heavyweight fighter, an astronaut, a superhero, a thoroughbred, a home-run hitter, a waitress juggling ‘16 entrees, 42 starters, 16 desserts,’ a jazz virtuoso, LeBron James, Magellan, Snuffleupagus. The actress Laurie Metcalf has been compared to all of these things.”",
  },
  {
    sections: ["politics", "business"],
    image: "https://live.staticflickr.com/3593/3778730672_0991c46cf3_z.jpg",
    title:
      "Flint Residents Learn To Harness Superpowers, But Trump Gets Away Again",
    description:
      "FLINT—They developed superpowers after years of drinking from a lead-poisoned water supply. But just having incredible abilities doesn't make them superheroes. Not yet.",
  },
  {
    sections: ["business", "culture", "people"],
    title:
      "Once Thriving Cheeto Driven To Extinction By Unregulated Snack Food Industry",
    description:
      "CHESTER, IA—Where once the cheeto could be found in great numbers, filling the crisp autumn air with a dangerously cheesy aroma, today there isn’t a trace of fluorescent orange to be seen, as overharvesting has all but eliminated the",
    image: "https://live.staticflickr.com/6007/5906449345_ff59237385_z.jpg",
  },
  {
    sections: ["home", "sports", "politics"],
    title: "The Same River Twice",
    description: "The story of rivers and relationships.",
  },
];
