import React from "react";
import { cleanup, render } from "@testing-library/react";
import "jest-styled-components";

import { Background } from "components/Header/Background";

afterEach(cleanup);

test("It renders element with specified background color", () => {
  const { container } = render(<Background color="white" />);
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild).toHaveStyleRule("background-color", "white");
});

test("It renders element with gradient background if color is object", () => {
  const { container } = render(
    <Background color={{ from: "black", to: "white" }} />
  );
  expect(container.firstChild).toMatchSnapshot();
  expect(container.firstChild).toHaveStyleRule(
    "background",
    "linear-gradient(90deg,black 0%,white 100%)"
  );
});
