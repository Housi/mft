import React from "react";
import { cleanup, render } from "@testing-library/react";
import App from "./App";

afterEach(cleanup);

test("It renders header and article list", () => {
  const { container } = render(<App />);
  expect(container.firstChild.childNodes.length).toBe(2);
});
