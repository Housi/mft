import * as React from "react";

function MenuIcon(props) {
  return (
    <svg
      preserveAspectRatio="xMinYMin meet"
      viewBox="0 0 32 32"
      fill="none"
      stroke="currentColor"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
      {...props}
    >
      <path d="M7 16h18M7 10h18M7 22h18" />
    </svg>
  );
}

export default MenuIcon;
